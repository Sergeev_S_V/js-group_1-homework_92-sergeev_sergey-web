export const FETCH_IMAGE_SUCCESS = 'FETCH_IMAGE_SUCCESS';
export const MOUSE_DOWN = 'MOUSE_DOWN';
export const MOUSE_UP = 'MOUSE_UP';
export const DRAWING = 'DRAWING';
