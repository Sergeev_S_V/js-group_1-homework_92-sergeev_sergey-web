import React, {Component} from 'react';
import '../../App.css';
import {connect} from "react-redux";
import {drawing, mouseDown, mouseUp} from "./actions";

class App extends Component {

  componentDidMount() {
    this.websocket = new WebSocket('ws://localhost:8000/chat');

    this.websocket.onmessage = (message) => {
      const decodedMessage = JSON.parse(message.data);

      switch (decodedMessage.type) {
        case 'NEW_IMAGE':
          this.context = this.canvas.getContext('2d');
          this.imageData = this.context.createImageData(1, 1);
          this.data = this.imageData.data;
          this.data[0] = 0;
          this.data[1] = 0;
          this.data[2] = 0;
          this.data[3] = 255;
          decodedMessage.pixels.forEach(pixel => (
            this.context.putImageData(this.imageData, pixel.x, pixel.y)
        ));
          break;
        default:
          return null;
      }
    }
  }


  canvasMouseMoveHandler = event => {

    if (this.props.mouseDown) {
      event.persist();

      const pixels = {
        x: event.clientX,
        y: event.clientY
      };

      this.props.onDrawing(pixels);

      this.context = this.canvas.getContext('2d');
      this.imageData = this.context.createImageData(1, 1);
      this.data = this.imageData.data;
      this.data[0] = 0;
      this.data[1] = 0;
      this.data[2] = 0;
      this.data[3] = 255;

      this.context.putImageData(this.imageData, event.clientX, event.clientY);
    }
  };

  mouseDownHandler = event => {
    this.props.onMouseDown();
  };

  mouseUpHandler = event => {
    let pixels = this.props.pixels;

    pixels = JSON.stringify({
      type: 'CREATE_IMAGE',
      pixels: [...pixels]
    });

    this.props.onMouseUp();
    this.websocket.send(pixels);
  };

  render() {
    return (
      <div>
        <canvas
          ref={elem => this.canvas = elem}
          style={{border: '1px solid black'}}
          width={800}
          height={600}
          onMouseDown={this.mouseDownHandler}
          onMouseUp={this.mouseUpHandler}
          onMouseMove={this.canvasMouseMoveHandler}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  mouseDown: state.mouseDown,
  pixels: state.pixels
});

const mapDispatchToProps = dispatch => ({
  onMouseDown: () => dispatch(mouseDown()),
  onMouseUp: () => dispatch(mouseUp()),
  onDrawing: pixels => dispatch(drawing(pixels)),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
