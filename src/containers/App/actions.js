import {DRAWING, FETCH_IMAGE_SUCCESS, MOUSE_DOWN, MOUSE_UP} from "./actionTypes";

export const fetchImage = pixels => ({
  type: FETCH_IMAGE_SUCCESS, pixels
});

export const mouseUp = () => ({
  type: MOUSE_UP
});

export const mouseDown = () => ({
  type: MOUSE_DOWN
});

export const drawing = pixels => ({
  type: DRAWING, pixels
});