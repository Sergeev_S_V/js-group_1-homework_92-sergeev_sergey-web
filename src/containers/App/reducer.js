import {DRAWING, FETCH_IMAGE_SUCCESS, MOUSE_DOWN, MOUSE_UP} from "./actionTypes";

const initialState = {
  pixels: [],
  mouseDown: false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case MOUSE_UP:
      return {...state, mouseDown: false, pixels: []};
    case MOUSE_DOWN:
      return {...state, mouseDown: true};
    case DRAWING:
      return {...state, pixels: state.pixels.concat(action.pixels)};
    case FETCH_IMAGE_SUCCESS:
      console.log(action.pixels)
      return {...state, pixels: action.pixels};
    default:
      return state;
  }
};

export default reducer;